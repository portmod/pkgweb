# Copyright 2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""
Creates pages for packages
"""

import os
from functools import cmp_to_key
from typing import Dict, List

from jinja2 import Template
from portmod.query import get_flags, get_maintainer_string
from portmod.repo import get_repo_name
from portmod.repo.metadata import get_package_metadata
from portmod.util import get_newest
from portmodlib.atom import Atom, version_gt
from portmodlib.fs import ci_exists
from portmodlib.parsers.list import read_list
from portmodlib.usestr import use_reduce

from . import SiteInfo, group_packages

TEMPLATE = Template(
    """
<div id="grid">
  {{ sitemenu }}
  <div id="repomenu">
    <ll>
{% if wiki is defined %}
    <li><a href="{{ wiki }}">Wiki</a></li>
{% endif %}
    <li><a href="{{ issues }}">Issues</a></li>
    <li><a href="{{ mrs }}">Merge Requests</a></li>
    <li><a href="{{ tree }}">Tree</a></li>
    <li><a href="{{ history }}">History</a></li>
  </ll>
    </div>

<div id="modmain">
<main>
    <h2><a href="..">{{ category }}</a>/{{ package_name }}</h2>
    <h1>{{ name }}</h1>
    <p>{{ desc }}</p>
{% if homepage is defined %}
    <p><a href="{{ homepage }}">{{ homepage }}</a></p>
{% endif %}
<table>
    <tr>
        <td>Version</td>
        {% for arch in archs %}
        <td>{{ arch }}</td>
        {% endfor %}
    </tr>
{% for version in versions %}
  <tr>
      <td><a href="{{ tree }}/{{ pn }}-{{ version }}.pybuild">{{ version }}</a></td>
      {% for keyword in keywords[version] %}
            <td>{{ keyword }}</td>
      {% endfor %}
  </tr>
{% endfor %}
</table>

<table>
{% if longdescription is defined %}
<tr>
    <td>Long description</td>
    <td>{{ longdescription }}</td>
</tr>
{% endif %}

{% if other_homepages is defined %}
<tr>
    <td>Other Homepages</td>
    <td>
      {% for url in other_homepages %}
        <p><a href="{{ url }}">{{ url }}</a></p>
      {% endfor %}
    </td>
</tr>
{% endif %}
{% if tags is defined %}
<tr>
    <td>Tags </td>
    <td>
      {% for tag in tags %}
        <a href="{{ site_root }}/tags/{{ tag }}">{{ tag }}</a>
      {% endfor %}
    </td>
</tr>
{% endif %}

<tr>
    <td>USE flags</td>
    <td>
{% if local_use is defined %}
    <b>Local USE flags</b>
    <table>
  {% for flag in local_use %}
    <tr>
        <td><a href={{ site_root }}/flags/{{ flag }}>{{ flag }}</a></td>
        <td>{{ local_use[flag] }}</td>
    </p>
  {% endfor %}
    </table>
{% endif %}
{% if global_use is defined %}
    <b>Global USE flags</b>
    <table>
  {% for flag in global_use %}
    <tr>
        <td><a href={{ site_root }}/flags/{{ flag }}>{{ flag }}</a></td>
        <td>{{ global_use[flag] }}</td>
    </tr>
  {% endfor %}
    </table>
{% endif %}
{% if use_expand is defined %}
  {% for typ in use_expand %}
    <b>USE_EXPAND flags ({{ typ }})</b>
    <table>
  {% for flag in use_expand[typ] %}
    <tr>
        <td>{{ flag }}</td>
        <td>{{ use_expand[typ][flag] }}</td>
    </tr>
  {% endfor %}
    </table>
  {% endfor %}
{% endif %}
</tr>
{% if license is defined %}
<tr>
    <td>License</td>
    <td>{{ license }}</td>
</tr>
{% endif %}
{% if maintainer is defined %}
<tr>
    <td>Maintainer</td>
    <td>{{ maintainer }}</td>
{% endif %}
{% if upstream is defined %}
<tr>
    <td>Upstream</td>
    <td>
    <table>
        {% if upstream.get("maintainer") != None %}
        <tr>
            <td>Author/Maintainer</td>
            <td>{{ upstream["maintainer"] }}</td>
        </tr>
        {% endif %}
        {% if upstream.get("bugs-to") != None %}
        <tr>
            <td>Bugs To</td>
            <td>
            <a href="{{ upstream["bugs-to"] }}">{{ upstream["bugs-to"] }}</a>
            </td>
        </tr>
        {% endif %}
        {% if upstream.get("doc") != None %}
        <tr>
            <td>Documentation</td>
            <td>
            <a href="{{ upstream["doc"] }}">{{ upstream["doc"] }}</a>
            </td>
        </tr>
        {% endif %}
        {% if upstream.get("changelog") != None %}
        <tr>
            <td>Changelog</td>
            <td>
            <a href="{{ upstream["changelog"] }}">{{ upstream["changelog"] }}</a>
            </td>
        </tr>
        {% endif %}
    </table>
    </td>
</tr>
{% endif %}
</table>
</main>
</div
</div
"""
)


def create_package_pages(info: SiteInfo):
    grouped = group_packages(info.repo_root)

    archs = sorted(read_list(os.path.join(info.repo_root, "profiles", "arch.list")))

    os.makedirs(info.output, exist_ok=True)

    for group, pkgs in grouped.items():
        # Collect version and keyword for each package version
        # Use name, desc and homepage from most recent version
        # Collect package metadata
        # Generate link for searching the bug tracker

        pkgs = list(reversed(sorted(pkgs, key=lambda x: x.version)))
        versions = []
        keywords: Dict[str, List[str]] = {}
        for pkg in pkgs:
            version = pkg.ATOM.version.display()
            versions.append(version)
            keywords[version] = []
            for arch in archs:
                if arch in pkg.KEYWORDS:
                    keywords[version].append("stable")
                elif "~" + arch in pkg.KEYWORDS:
                    keywords[version].append("unstable")
                elif "-" + arch in pkg.KEYWORDS:
                    keywords[version].append("unsupported")
                else:
                    keywords[version].append("")

        newest = get_newest(pkgs)
        metadata = get_package_metadata(newest)

        homepages = use_reduce(newest.HOMEPAGE, matchall=True, flat=True)

        local_use = {}
        global_use = {}
        use_expand = {}
        for pkg in pkgs:
            loc, glob, exp = get_flags(pkg)
            local_use.update(loc)
            global_use.update(glob)
            use_expand.update(exp)

        optionals = {}

        # TODO: Use flags should link to page showing all packages using that flag
        if local_use:
            optionals["local_use"] = local_use
        if global_use:
            optionals["global_use"] = global_use
        if use_expand:
            optionals["use_expand"] = use_expand

        if homepages:
            optionals["homepage"] = homepages[0]
        if len(homepages) > 1:
            optionals["other_homepages"] = homepages[1:]

        # FIXME: Should include a link to the license in tree
        if newest.LICENSE:
            optionals["license"] = newest.LICENSE

        if metadata:
            if metadata.longdescription:
                optionals["longdescription"] = metadata.longdescription
            if metadata.tags:
                optionals["tags"] = metadata.tags
            if metadata.maintainer:
                optionals["maintainer"] = get_maintainer_string(metadata.maintainer)
            if metadata.upstream:
                optionals["upstream"] = {}
                if metadata.upstream.maintainer:
                    optionals["upstream"]["maintainer"] = get_maintainer_string(
                        metadata.upstream.maintainer
                    )
                if metadata.upstream.doc:
                    optionals["upstream"]["doc"] = metadata.upstream.doc
                if metadata.upstream.bugs_to:
                    optionals["upstream"]["bugs-to"] = metadata.upstream.bugs_to
                if metadata.upstream.changelog:
                    optionals["upstream"]["changelog"] = metadata.upstream.changelog

        repo = get_repo_name(info.repo_root)
        if ci_exists(
            os.path.join(repo, "pkg", newest.PN + ".md"),
            prefix=os.environ["PORTMOD_WIKI"],
        ):
            optionals[
                "wiki"
            ] = f"https://gitlab.com/portmod/portmod/-/wikis/{repo}/pkg/{newest.PN}"

        branch = info.get_default_branch()

        html = TEMPLATE.render(
            pn=newest.PN,
            category=Atom(group).C,
            package_name=Atom(group).PN,
            name=newest.NAME,
            desc=newest.DESC,
            issues=f"{info.gitlab_url}/-/issues?scope=all&search={group}",
            mrs=f"{info.gitlab_url}/-/merge_requests?scope=all&search={group}",
            tree=f"{info.gitlab_url}/-/tree/{branch}/{group}",
            history=f"{info.gitlab_url}/-/commits/{branch}/{group}",
            versions=versions,
            keywords=keywords,
            archs=archs,
            site_root=info.site_root,
            sitemenu=info.menu(),
            **optionals,
        )

        os.makedirs(os.path.dirname(os.path.join(info.output, group)), exist_ok=True)
        with open(os.path.join(info.output, group + ".html"), "w") as file:
            file.write(html)

        with open("stork.toml", "a") as file:
            title = newest.NAME.replace('"', '\\"')
            print("[[input.files]]", file=file)
            print(f'path = "{group}.html"', file=file)
            print(f'url = "{info.site_root}/{group}/"', file=file)
            print(f'title = "{title}"', file=file)
