# Copyright 2022 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

import os
from typing import Dict, Optional

from jinja2 import Template
from portmod.repo.metadata import get_global_use, get_package_metadata

from . import SiteInfo, group_packages

TEMPLATE = Template(
    """
  {{ sitemenu }}
<div id="modmain">
<main>
    <h2>{% if global_flag %}Global{% else %}Local{% endif %} USE Flag</h2>
    <h1>{{ flag }}</h1>
{% if desc %}
    <p>{{ desc }}</p>
{% endif %}
{% if local_packages %}
<table>
    <tr>
        <td>Package</td>
        <td>"{{ flag }}" Flag Description</td>
    </tr>
{% for package in local_packages %}
  <tr>
        <td><a href="{{ site_root }}/{{ package }}">{{ package }}</a></td>
        <td>{{ local_descs[package] }}</td>
  </tr>
{% endfor %}
</table>
{% endif %}

{% if global_flag %}
<h4>All packages providing a "{{ flag }}" USE flag</h4>
<table>
{% for package in global_packages %}
  <tr>
        <td><a href="{{ site_root }}/{{ package }}">{{ package }}</a></td>
  </tr>
{% endfor %}
</table>
{% endif %}

</main>
</div
</div
"""
)

INDEX_TEMPLATE = Template(
    """
  {{ sitemenu }}
<div id="modmain">
<main>
    <h2>Global USE Flags</h2>
<table>
{% for flag in flags %}
  <tr>
        <td><a href="{{ flag }}">{{ flag }}</a></td>
        <td>{{ descs[flag] }}</td>
  </tr>
{% endfor %}
</table>

</main>
</div
</div
"""
)


def create_flag_pages(info: SiteInfo):
    global_use = get_global_use(info.repo_root)

    for flag, desc in global_use.items():
        create_flag_page(flag, info, desc=desc, global_flag=True)

    local_use = set()

    for _, pkgs in group_packages(info.repo_root).items():
        use_descs = {}
        for pkg in pkgs:
            metadata = get_package_metadata(pkg)
            if metadata:
                use_descs.update(metadata.use)
        for flag in use_descs:
            if flag not in global_use and flag not in local_use:
                local_use.add(flag)
                create_flag_page(flag, info, global_flag=False)

    create_flag_index(global_use, info)


def create_flag_index(global_use: Dict[str, str], info: SiteInfo):
    html = INDEX_TEMPLATE.render(
        flags=sorted(global_use.keys()),
        descs=global_use,
        site_root=info.site_root,
        sitemenu=info.menu(),
    )

    os.makedirs(os.path.join(info.output, "flags"), exist_ok=True)
    with open(os.path.join(info.output, "flags/index.html"), "w", encoding="utf-8") as file:
        file.write(html)

    with open("stork.toml", "a", encoding="utf-8") as file:
        print("[[input.files]]", file=file)
        print('path = "flags/index.html"', file=file)
        print(f'url = "{info.site_root}/flags/"', file=file)
        print('title = "Global USE Flags"', file=file)


def create_flag_page(flag: str, info: SiteInfo, *, desc: Optional[str] = None, global_flag: bool):
    global_packages = set()
    local_packages = []
    local_descs = {}
    for group, pkgs in group_packages(info.repo_root).items():
        use_descs = {}
        for pkg in pkgs:
            metadata = get_package_metadata(pkg)
            if metadata:
                use_descs.update(metadata.use)
            if flag in pkg.IUSE_EFFECTIVE:
                global_packages.add(pkg.CPN)
        if use_descs.get(flag):
            local_packages.append(group)
            local_descs[group] = use_descs.get(flag)

    html = TEMPLATE.render(
        flag=flag,
        desc=desc,
        site_root=info.site_root,
        global_packages=sorted(global_packages),
        local_packages=sorted(local_packages),
        local_descs=local_descs,
        global_flag=global_flag,
        sitemenu=info.menu(),
    )

    os.makedirs(
        os.path.dirname(os.path.join(info.output, "flags", flag)), exist_ok=True
    )
    with open(os.path.join(info.output, f"flags/{flag}.html"), "w", encoding="utf-8") as file:
        file.write(html)

    with open("stork.toml", "a", encoding="utf-8") as file:
        print("[[input.files]]", file=file)
        print(f'path = "flags/{flag}.html"', file=file)
        print(f'url = "{info.site_root}/flags/{flag}/"', file=file)
        print(f'title = "{flag}"', file=file)
