import glob
import os
import shutil
import sys
from datetime import date
from subprocess import PIPE, Popen
from typing import Dict, List, Tuple
from logging import error

import git
from jinja2 import Template
from portmod.globals import env
from portmod.loader import load_file
from portmod.pybuild import Pybuild
from portmod.repo import Repo, get_repo_name
from portmod.repo.metadata import get_package_metadata

from pkgweb import SiteInfo, group_packages
from pkgweb.index import create_package_indexes
from pkgweb.packages import create_package_pages
from pkgweb.flags import create_flag_pages


def get_repo_stats(root: str) -> Dict[str, int]:
    grouped = group_packages(root)
    orphaned = 0
    for pkgs in grouped.values():
        metadata = get_package_metadata(pkgs[0])
        if not metadata or not metadata.maintainer:
            orphaned += 1
    return {
        "Total Packages": len(grouped),
        "Orphaned Packages": orphaned,
    }


def get_recent_packages(root: str) -> List[Tuple[Pybuild, str]]:
    repo = git.Repo.init(root)

    recent: List[Pybuild] = []
    recent_names = set()
    print("Searching for recently updated packages...")
    for commit in repo.iter_commits():
        # Ignore merge commits
        if len(commit.parents) == 1:
            print(commit.message)
            for file, filestats in commit.stats.files.items():
                print(file, filestats)
                if not os.path.exists(os.path.join(root, file)):
                    print("Does not exist, skipping")
                    continue
                if len(recent) >= 10:
                    return recent
                if (
                    file.endswith(".pybuild")
                    and os.path.dirname(file) not in recent_names
                ):
                    print(f"Checking package {file}")
                    try:
                        pkg = load_file(os.path.join(root, file))
                    except Exception as e:
                        error(e)
                    else:
                        recent_names.add(os.path.dirname(file))
                        recent.append(
                            (
                                pkg,
                                date.fromtimestamp(commit.authored_date),
                            )
                        )
                        print(f"Found recent file {len(recent)}")
                        print(
                            date.fromtimestamp(commit.authored_date),
                            str(commit.authored_datetime.date()),
                            str(commit.committed_datetime.date()),
                        )

    return recent


if __name__ == "__main__":
    root = os.path.abspath(sys.argv[1])
    # FIXME: Loading packages shouldn't require state
    env.REPOS.append(Repo(location=root, name=get_repo_name(root)))
    gitlab_url = sys.argv[2]
    site_name = sys.argv[3]

    info = SiteInfo(
        name=site_name,
        gitlab_url=gitlab_url,
        repo_root=root,
        site_root=os.environ["SITE_ROOT"],
    )

    # Create base stork index
    # This will be added to as package pages are created
    with open("stork.toml", "w") as file:
        print("[input]", file=file)
        print(f'base_directory = "{info.output}"', file=file)
        print('title_boost = "Large"', file=file)

    create_package_pages(info)
    create_package_indexes(info)
    create_flag_pages(info)

    main_file = Template(
        """{{ menu }}
    <main>
    <div id="grid">
    <div id="repomenu">
        <table>
        <thead>
            <th>Statistics</th>
        </thead>
        {% for name, value in stats.items() %}
        <tr>
            <td>{{ name }}</td>
            <td>{{ value }}</td>
        </tr>
        {% endfor %}
        </table>

        <table>
        <thead>
            <th>Recently Updated Packages</th>
        </thead>
        {% for pkg, date in recent %}
        <tr>
            <td><a href={{ pkg.CPN }}>{{ pkg.CPN }}</a></td>
            <td>{{ date }}</td>
        </tr>
        {% endfor %}
        </table>
        </div>
        <div id="modmain">
        {{ readme }}
        </div>
        </div>
    </main>
        """
    )

    # If no readme is present, just display the name
    readme = f"<h1>{info.name}</h1>"
    if shutil.which("pandoc"):
        for filename in glob.glob(os.path.join(root, "README*")):
            p = Popen(["pandoc", filename, "-t", "html"], stdout=PIPE)
            readme = p.communicate()[0].decode()
            break

    with open(os.path.join(info.output, "index.html"), "w") as file:
        file.write(
            main_file.render(
                menu=info.menu(),
                name=info.name,
                stats=get_repo_stats(root),
                recent=get_recent_packages(root),
                readme=readme,
            )
        )
