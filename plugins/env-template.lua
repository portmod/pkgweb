var_names = config["env_vars"]
tmpl = config["template"]
selector = config["selector"]
action = config["action"]

env = {}

function add_var(name)
  local value = getenv(name)
  -- getenv() returns nil rather than "" for undefined variables.
  -- In Lua setting a key to nil is equivalent to removing it from a table.
  -- Thus we need to force it to "" to prevent possible type errors.
  if not value then
    value = ""
  end
  env[name] = value

  Log.debug("Retrieved environment variable %s=\"%s\"", name, value)
end
Table.iter_values(add_var, var_names)

html_str = String.render_template(tmpl, env)
html = HTML.parse(html_str)

parent = HTML.select_one(page, selector)
HTML.append_child(parent, html)
