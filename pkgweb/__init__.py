from dataclasses import dataclass
from typing import Dict, List, Optional
import subprocess
import os

from portmod.loader import load_all
from portmod.pybuild import Pybuild


@dataclass
class SiteInfo:
    name: str
    repo_root: str
    site_root: str
    gitlab_url: str
    output: str = "site"
    _default_branch: Optional[str] = None

    def menu(self):
        return f"""
      <div id="sitemenu">
        <ul>
          <li><a href="{self.site_root}">Home</a></li>
          <li><a href="{self.site_root}/categories">Categories</a></li>
          <li><a href="{self.site_root}/tags">Tags</a></li>
          <li><a href="{self.site_root}/flags">USE Flags</a></li>
          <li><a href="{self.gitlab_url}">Repository Home</a></li>
        <ul>
      </div>
    """

    def get_default_branch(self) -> Optional[str]:
        if self._default_branch is None:
            proc = subprocess.run(
                ["git", "remote", "show"],
                stdout=subprocess.PIPE,
                cwd=self.repo_root,
                check=True,
                encoding="utf-8",
            )
            remotes = proc.stdout.splitlines()
            for remote in remotes:
                proc = subprocess.run(
                    ["git", "remote", "show", remote],
                    stdout=subprocess.PIPE,
                    cwd=self.repo_root,
                    check=True,
                    encoding="utf-8",
                )
                for line in proc.stdout.splitlines():
                    if line.strip().startswith("HEAD branch:"):
                        self._default_branch = line.replace("HEAD branch:", "").strip() 
                        break
                break
        return self._default_branch


def group_packages(root: str) -> Dict[str, List[Pybuild]]:
    grouped: Dict[str, List[Pybuild]] = {}

    for pkg in load_all(only_repo_root=root):
        if pkg.CPN in grouped:
            grouped[pkg.CPN].append(pkg)
        else:
            grouped[pkg.CPN] = [pkg]
    return grouped
