#!/bin/bash

# Dependencies are:
# stork: https://stork-search.net/docs/install
# soupault: https://soupault.app/#downloads
# python3+jinja2: https://pypi.org/project/Jinja2/

if [[ $# != 4 ]]; then
    echo "Usage $0 <repo_path> <gitlab_url> <site_title> <site_root_url>"
    exit
fi

repo=$1
site=$2
name=$3
export SITE_ROOT=$4

git clone https://gitlab.com/portmod/portmod-wiki.git || (cd portmod-wiki && git pull && cd ..)
export PORTMOD_WIKI=`pwd`/portmod-wiki

python -c "import jinja2" && python -c "import portmod"
if [ $? -ne 0 ]; then
    virtualenv .pkgweb-venv
    source .pkgweb-venv/bin/activate
    pip install --upgrade jinja2 portmod
fi
python main.py "$repo" "$site" "$name" || exit 1

stork_ver=1.6.0
soupault_ver=3.2.0
stork=`which stork || echo ./stork`
if [[ ! -r $stork ]] || [[ `$stork --version` != "Stork ${stork_ver}" ]]; then
    wget https://github.com/jameslittle230/stork/releases/download/v${stork_ver}/stork.js -O site/stork.js
    wget https://github.com/jameslittle230/stork/releases/download/v${stork_ver}/stork.wasm -O site/stork.wasm
    wget https://github.com/jameslittle230/stork/releases/download/v${stork_ver}/stork-ubuntu-22-04 -O stork
    chmod +x stork
    stork=./stork
fi
if [[ ! -r soupault && ! `which soupault` ]] || [[ `./soupault --version | head -n 1` != "soupault ${soupault_ver}" ]]; then
    ver=${soupault_ver}
    wget https://files.baturin.org/software/soupault/${ver}/soupault-${ver}-linux-x86_64.tar.gz
    tar -xzf soupault-${ver}-linux-x86_64.tar.gz
    mv soupault-${ver}-linux-x86_64/soupault .
    rm -r soupault-${ver}-linux-x86_64
    rm soupault-${ver}-linux-x86_64.tar.gz
    chmod +x soupault
fi
soupault=`which soupault || echo ./soupault`
echo $soupault
$soupault || exit 1

stork_command="$stork build --input stork.toml --output build/index.st"
echo $stork_command
$stork_command || exit 1
gzip -f -k -6 $(find build -type f)
