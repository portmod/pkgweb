# Copyright 2021 Portmod Authors
# Distributed under the terms of the GNU General Public License v3

"""
Creates category and package indexes
"""

import os
from collections import defaultdict
from typing import Dict, Iterable, Optional, List

from jinja2 import Template
from portmod.repo.metadata import get_category_metadata, get_package_metadata
from portmod.util import get_newest
from portmod.pybuild import Pybuild

from . import SiteInfo, group_packages


def create_package_index(
    info: SiteInfo, packages: List[Pybuild], category: str, desc: Optional[str] = None, parentdir="."
):
    template = Template(
        """
        {{ sitemenu }}
    <main>
    <h1>{{ category }}</h1>
    {% if desc %}
    <p>{{ desc }}</p>
    {% endif %}

    <table>
      <thead>
        <th>Package Atom</th>
        <th>Name</th>
        <th>Description</th>
      </thead>
      <tbody>
    {% for package in packages %}
      <tr>
          <td><a href="{{ parentdir }}/{{ package.CATEGORY }}/{{ package.PN }}">{{ package.PN }}</a></td>
          <td>{{ package.NAME }}</td>
          <td>{{ package.DESC }}</td>
      </tr>
    {% endfor %}
      </tbody>
    </table>

    </main>
    """
    )

    html = template.render(
        packages=sorted(packages, key=lambda x: x.PN),
        category=category,
        desc=desc,
        sitemenu=info.menu(),
        parentdir=parentdir,
    )
    return html


def create_package_indexes(info: SiteInfo):
    grouped = group_packages(info.repo_root)
    categories = defaultdict(list)
    tags = defaultdict(list)

    for atom, pkgs in grouped.items():
        newest = get_newest(pkgs)
        categories[newest.CATEGORY].append(newest)
        metadata = get_package_metadata(newest)
        if metadata:
          for tag in metadata.tags:
              tags[tag].append(newest)

    categorydesc = {}

    for category, pkgs in categories.items():
        metadata = get_category_metadata(info.repo_root, category)
        if metadata:
            desc = metadata.longdescription
        else:
            desc = ""

        categorydesc[category] = desc

        os.makedirs(os.path.join(info.output, category), exist_ok=True)
        with open(os.path.join(info.output, category, "index.html"), "w") as file:
            file.write(create_package_index(info, pkgs, category, desc, parentdir=".."))

    for tag, pkgs in tags.items():
        os.makedirs(os.path.join(info.output, "tags", tag), exist_ok=True)
        with open(os.path.join(info.output, "tags", tag, "index.html"), "w") as file:
            file.write(create_package_index(info, pkgs, tag, parentdir="../.."))

    create_category_index(info, categorydesc)
    create_tag_index(info, tags.keys())


def create_category_index(info: SiteInfo, categorydesc: Dict[str, str]):
    index_template = Template(
        """
    {{ sitemenu }}
    <main>
    <h1>Categories</h1>
    <table>
    {% for category, desc in categories %}
      <tr>
          <td><a href="../{{ category }}">{{ category }}</a></td>
          <td>{{ desc }}</td>
      </tr>
    {% endfor %}
    </table>
    </main>
        """
    )

    html = index_template.render(
        categories=sorted(categorydesc.items(), key=lambda x: x[0]),
        site_name=info.name,
        sitemenu=info.menu(),
    )
    with open(os.path.join(info.output, "categories.html"), "w") as file:
        file.write(html)


def create_tag_index(info: SiteInfo, tags: Iterable[str]):
    index_template = Template(
        """
    {{ sitemenu }}
    <main>
    <h1>Tags</h1>
    <table>
    {% for tag in tags %}
      <tr>
          <td><a href="{{ tag }}">{{ tag }}</a></td>
      </tr>
    {% endfor %}
    </table>
    </main>
        """
    )

    html = index_template.render(
        tags=sorted(tags),
        site_name=info.name,
        sitemenu=info.menu(),
    )
    with open(os.path.join(info.output, "tags.html"), "w") as file:
        file.write(html)
